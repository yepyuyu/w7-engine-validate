<?php

namespace Itwmw\Validate\Providers\Rangine;

use Itwmw\Validate\Providers\Laravel\PresenceVerifier;
use Itwmw\Validate\Support\Storage\ValidateConfig;
use Itwmw\Validation\Factory;
use W7\Core\Provider\ProviderAbstract;
use W7\Facade\Config;
use W7\Facade\Container;

class ValidateProvider extends ProviderAbstract
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function boot()
    {
        $factory = new Factory(null, str_replace('-', '_', Config::get('app.setting.lang', 'zh_cn')));
        $factory->setPresenceVerifier(new PresenceVerifier(Container::get('db-factory')));
        ValidateConfig::instance()->setFactory($factory);
    }
}
