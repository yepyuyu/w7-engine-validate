<?php

namespace Itwmw\Validate;

use Closure;
use InvalidArgumentException;
use Itwmw\Validate\Exception\ValidateException;
use Itwmw\Validate\Exception\ValidateRuntimeException;
use Itwmw\Validate\Support\Common;
use Itwmw\Validate\Support\Concerns\MessageProviderInterface;
use Itwmw\Validate\Support\Concerns\ProcessorInterface;
use Itwmw\Validate\Support\DataAttribute;
use Itwmw\Validate\Support\Event\ValidateEventAbstract;
use Itwmw\Validate\Support\MessageProvider;
use Itwmw\Validate\Support\Processor\DataProcessor;
use Itwmw\Validate\Support\Processor\ProcessorExecCond;
use Itwmw\Validate\Support\Processor\ProcessorOptions;
use Itwmw\Validate\Support\Processor\ProcessorParams;
use Itwmw\Validate\Support\Processor\ProcessorSupport;
use Itwmw\Validate\Support\Processor\ProcessorValueType;
use Itwmw\Validate\Support\Rule\BaseRule;
use Itwmw\Validate\Support\Storage\ValidateConfig;
use Itwmw\Validate\Support\ValidateScene;
use Itwmw\Validation\Factory;
use Itwmw\Validation\Support\Collection\Collection;
use Itwmw\Validation\Support\Rule\AndRule;
use Itwmw\Validation\Support\Rule\OrRule;
use Itwmw\Validation\Support\Str;
use Itwmw\Validation\Support\ValidationException;
use Itwmw\Validation\ValidationData;
use Throwable;

class Validate
{
    /**
     * 全部原始验证规则.
     *
     * @see https://v.itwmw.com/6/Validate.html#rule
     *
     * @var array
     */
    protected $rule = [];

    /**
     * 验证场景定义.
     *
     * @see https://v.itwmw.com/6/Validate.html#scene
     *
     * @var array<string, array<string>|ValidateScene>
     */
    protected $scene = [];

    /**
     * 验证字段的名称.
     *
     * @see https://v.itwmw.com/6/Validate.html#customattributes
     *
     * @var array
     */
    protected $customAttributes = [];

    /**
     * 验证失败的错误消息.
     *
     * @see https://v.itwmw.com/6/Validate.html#message
     *
     * @var array
     */
    protected $message = [];

    /**
     * 当前类中自定义方法验证失败后的错误消息.
     *
     * @see https://v.itwmw.com/6/Validate.html#rulemessage
     *
     * @var array
     */
    protected $ruleMessage = [];

    /**
     * 预定义正则表达式验证规则.
     *
     * @see https://v.itwmw.com/6/Validate.html#regex
     *
     * @var array
     */
    protected $regex = [];

    /**
     * 验证规则组.
     *
     * @see https://v.itwmw.com/6/Validate.html#group
     *
     * @var array
     */
    protected $group = [];

    /**
     * 验证器下的全局事件.
     *
     * @see https://v.itwmw.com/6/Validate.html#event
     *
     * @var array
     */
    protected $event = [];

    /**
     * 为字段定义一个后置数据处理器，验证完毕后触发.
     *
     * @see https://v.itwmw.com/6/Validate.html#postprocessor
     *
     * @var array{string, mixed|array<mixed|ProcessorParams|ProcessorOptions|ProcessorValueType>}
     */
    protected $postprocessor = [];

    /**
     * 为字段定义一个后置数据处理器，验证之前触发.
     *
     * @see https://v.itwmw.com/6/Validate.html#preprocess
     *
     * @var array{string, mixed||array<mixed|ProcessorParams|ProcessorOptions|ProcessorValueType>}
     */
    protected $preprocessor = [];

    /**
     * 为验证失败设置一个自定义异常类.
     *
     * @see https://v.itwmw.com/6/Validate.html#exceptions
     *
     * @var array<class-string<Throwable>,string|array<string>>|class-string<Throwable>
     */
    protected $exceptions;

    /**
     * 事件优先级.
     */
    private bool $eventPriority = true;

    /**
     * 本次验证需要处理的事件.
     */
    private array $events = [];

    /**
     * 本次验证需要处理的前置场景验证事件.
     */
    private array $befores = [];

    /**
     * 本次验证需要处理的后置场景验证事件.
     */
    private array $afters = [];

    /**
     * 本次验证之前需要处理的数据处理器.
     *
     * @var array{string, mixed}
     */
    private array $preprocessors = [];

    /**
     * 本次验证之后需要处理的数据处理器.
     */
    private array $postprocessors = [];

    /**
     * 错误消息提供者.
     */
    private ?MessageProviderInterface $messageProvider = null;

    /**
     * 当前验证的原始数据.
     */
    private array $checkData = [];

    /**
     * 本次验证通过后的数据.
     */
    private array $validatedData = [];

    /**
     * 本次验证的字段名称.
     *
     * @var array<string>
     */
    private array $validateFields = [];

    /**
     * 当前验证场景名称.
     */
    private ?string $currentScene = null;

    /**
     * 使用正则表达式来验证的规则.
     *
     * @var array<string>
     */
    private array $regexRule = ['regex', 'not_regex'];

    /**
     * 扩展的普通规则.
     *
     * @see https://v.itwmw.com/6/Rule.html#extend-%E6%89%A9%E5%B1%95%E6%96%B9%E6%B3%95
     */
    private array $extensions = [];

    /**
     * 扩展的隐式规则.
     *
     * @see https://v.itwmw.com/6/Rule.html#extendimplicit-%E9%9A%90%E5%BC%8F%E6%89%A9%E5%B1%95
     */
    private array $implicitExtensions = [];

    /**
     * 扩展的依赖性规则.
     *
     * @see https://v.itwmw.com/6/Rule.html#extenddependent-%E4%BE%9D%E8%B5%96%E6%80%A7%E9%AA%8C%E8%AF%81%E5%99%A8
     */
    private array $dependentExtensions = [];

    /**
     * 扩展的错误信息替换器.
     *
     * @see https://v.itwmw.com/6/Rule.html#replacer-%E9%94%99%E8%AF%AF%E4%BF%A1%E6%81%AF%E6%9B%BF%E6%8D%A2%E5%99%A8
     */
    private array $replacers = [];

    /**
     * 是否初始化了扩展规则.
     */
    private bool $initExtendRule = false;

    /**
     * 自定义规则类
     *
     * @var array
     */
    private array $rulesClass = [];

    /**
     * @var array 等待验证的数据
     */
    protected array $withCheckData = [];

    /**
     * 创建验证器
     *
     * @param array $rules            验证规则
     * @param array $messages         错误消息
     * @param array $customAttributes 自定义字段名称
     */
    public static function make(array $rules = [], array $messages = [], array $customAttributes = []): static
    {
        return (new static())->setRules($rules)->setMessages($messages)->setCustomAttributes($customAttributes);
    }

    /**
     * Get Validator Factory.
     */
    private static function getValidationFactory(): Factory
    {
        return ValidateConfig::instance()->getFactory();
    }

    /**
     * 自动验证
     *
     * @param array $data 待验证的数据
     *
     * @throws ValidateException
     */
    public function check(array $data): array
    {
        $this->init();
        $this->checkData = $data;
        $this->addEvent('event', $this->event);
        $this->handleEvent($data, 'beforeValidate');
        $events       = $this->events;
        $this->events = [];
        $rule         = $this->getSceneRules();
        $data         = $this->pass($data, $rule);
        $this->events = $events;
        $this->handleEvent($data, 'afterValidate');
        return $data;
    }

    private function getSelfRules(): void
    {
        if ($this->initExtendRule) {
            return;
        }

        $methods = get_class_methods($this);
        foreach ($methods as $method) {
            if (str_starts_with($method, 'rule')) {
                $ruleName                    = substr($method, 4);
                $ruleName                    = Str::snake($ruleName);
                $this->extensions[$ruleName] = $this->$method(...);
            }
        }
        $this->initRuleMessage();
        $this->initExtendRule = true;
    }

    /**
     * 进行数据验证，并对数据进行处理
     *
     * @param array $data  待验证的数据
     * @param array $rules 验证规则
     *
     * @throws ValidateException
     */
    private function pass(array $data, array $rules): array
    {
        $this->withCheckData = $data;

        $this->preprocessors  = array_merge($this->preprocessor, $this->preprocessors);
        $this->postprocessors = array_merge($this->postprocessor, $this->postprocessors);

        // 已验证的字段不会被重新验证
        $checkFields = array_diff(array_keys($rules), $this->validateFields);
        $checkRules  = array_intersect_key($rules, array_flip($checkFields));
        $checkRules  = $this->getCheckRules($checkRules);

        $this->validateFields = array_merge($this->validateFields, $checkFields);

        // 预处理器只处理当前正在验证的字段
        $fields = array_keys($checkRules);
        $data   = $this->processData($data, $fields, $this->preprocessors);

        if ($this->eventPriority) {
            $this->handleEvent($data, 'beforeValidate');
            $this->handleEventCallback($data, 'before');
        } else {
            $this->handleEventCallback($data, 'before');
            $this->handleEvent($data, 'beforeValidate');
        }

        $validatedData = $this->validatedData;
        if (!empty($checkRules)) {
            try {
                $validator = $this->getValidationFactory()->make($data, $checkRules, $this->message, $this->customAttributes);
                $validator->addExtensions($this->extensions);
                $validator->addImplicitExtensions($this->implicitExtensions);
                $validator->addDependentExtensions($this->dependentExtensions);
                $validator->addReplacers($this->replacers);
                $validator->setFallbackMessages($this->ruleMessage);
                $validatedData = $validator->validate();
                $validatedData = array_merge($this->validatedData, $validatedData);
            } catch (ValidationException $e) {
                throw $this->getException($e);
            }
        }

        $validatedData = $this->processData($validatedData, $fields, $this->postprocessors);

        if ($this->eventPriority) {
            $this->handleEventCallback($validatedData, 'after');
            $this->handleEvent($validatedData, 'afterValidate');
        } else {
            $this->handleEvent($validatedData, 'afterValidate');
            $this->handleEventCallback($validatedData, 'after');
        }

        $this->initScene();
        $this->scene(null);
        return $validatedData;
    }

    /**
     * 解析规则 - 处理规则中的参数
     *
     * @param        $rule
     * @param string $field
     *
     * @return array|mixed|string
     */
    protected function parseRule($rule, string $field): mixed
    {
        if ($rule instanceof AndRule) {
            $rule->rules = array_map(function ($rule) use ($field) {
                return $this->parseRule($rule, $field);
            }, $rule->rules);
        }

        if (is_string($rule)) {
            // 解析正则表达式规则引用
            foreach ($this->regexRule as $regexRuleName) {
                $regexRuleName = $regexRuleName . ':';
                if (str_starts_with($rule, $regexRuleName)) {
                    $regexName = substr($rule, strlen($regexRuleName));
                    if (isset($this->regex[$regexName])) {
                        return $regexRuleName . $this->regex[$regexName];
                    }
                }
            }

            // 获取自定义规则类
            $ruleClass = $this->getRuleClass($rule);
            if (false !== $ruleClass) {
                if (!empty($message = $this->getMessageProvider()->getMessage($field, $rule))) {
                    $ruleClass->setMessage($message);
                }
                return $ruleClass;
            }
        }

        return $rule;
    }

    /**
     * 从规则组中读取规则，不进行解析操作
     */
    private function getRuleFromRuleGroup(string $name): array|false
    {
        if (isset($this->group[$name])) {
            $ruleGroup = $this->group[$name];
            if ($ruleGroup instanceof OrRule || $ruleGroup instanceof AndRule) {
                return [$ruleGroup];
            }
            if (!is_array($ruleGroup)) {
                return explode('|', $ruleGroup);
            }
            return $ruleGroup;
        }

        return false;
    }

    protected function parseParams(?array $params): ?array
    {
        return $params;
    }

    /**
     * 将原始规则转为验证器规则.
     *
     * @param array|null $rules 原始规则，如果为null则获取全部的初始规则
     *
     * @return array|array[]
     */
    public function getCheckRules(?array $rules = null): array
    {
        if (is_null($rules)) {
            $rules = $this->getInitialRules();
        }
        $rulesFields = array_keys($rules);
        $rule        = array_map(function ($rules, $field) {
            if (!is_array($rules)) {
                $rules = explode('|', $rules);
            }

            $rules = array_reduce($rules, function ($rules, $rule) {
                if (is_string($rule) && ($ruleGroup = $this->getRuleFromRuleGroup($rule))) {
                    $rules = array_merge($rules, $ruleGroup);
                } else {
                    $rules[] = $rule;
                }
                return $rules;
            }, []);

            return array_map(function ($rule) use ($field) {
                if (is_string($rule)) {
                    if ('or' === $rule) {
                        throw new InvalidArgumentException('Validation rule or requires at least 1 parameters.');
                    }

                    if (str_starts_with($rule, 'or:')) {
                        return $this->parseOrRule($rule, $field);
                    }
                }

                return $this->parseRule($rule, $field);
            }, $rules);
        }, $rules, $rulesFields);

        return array_combine($rulesFields, $rule);
    }

    /**
     * 解析OrRule，并解析每个规则参数
     *
     * @param        $rule
     * @param string $field
     *
     * @return OrRule|mixed
     */
    protected function parseOrRule($rule, string $field): mixed
    {
        if (str_starts_with($rule, 'or:')) {
            $ruleInfo = $this->getKeyAndParam($rule, true);
            if (empty($ruleInfo[1])) {
                throw new InvalidArgumentException('Validation rule or requires at least 1 parameters.');
            }
            $orRules = [];
            foreach ($ruleInfo[1] as $item) {
                if (is_string($item)) {
                    $item = $this->parseOrRule($item, $field);
                    if ($ruleGroup = $this->getRuleFromRuleGroup($item)) {
                        $orRules = array_merge($orRules, $ruleGroup);
                    } else {
                        $orRules[] = $item;
                    }
                } else {
                    $orRules[] = $item;
                }
            }

            $orRules = array_map(function ($rule) use ($field) {
                return $this->parseRule($rule, $field);
            }, $orRules);

            return new OrRule($orRules);
        }

        return $rule;
    }

    /**
     * 获取原始规则.
     *
     * @param string|null $sceneName 场景名称，如果不提供则获取当前场景名称
     */
    public function getInitialRules(?string $sceneName = ''): array
    {
        if ('' === $sceneName) {
            $sceneName = $this->currentScene;
        }

        if (empty($sceneName)) {
            return $this->rule;
        }

        if (method_exists($this, 'scene' . Str::studly($sceneName))) {
            $scene = new ValidateScene($this->rule);
            call_user_func([$this, 'scene' . Str::studly($sceneName)], $scene);
            return $scene->getRules();
        }

        if (isset($this->scene[$sceneName])) {
            if (is_array($this->scene[$sceneName])) {
                return array_intersect_key($this->rule, array_flip($this->scene[$sceneName]));
            } elseif ($this->scene[$sceneName] instanceof ValidateScene) {
                return $this->scene[$sceneName]->getRules();
            } else {
                throw new InvalidArgumentException('The scene configuration is incorrect');
            }
        }

        return $this->rule;
    }

    /**
     * Get the instance class of a custom rule.
     *
     * @param string $ruleName Custom Rule Name
     *
     * @return false|BaseRule
     */
    private function getRuleClass(string $ruleName): BaseRule|false
    {

        [$ruleName, $params] = $this->getKeyAndParam($ruleName, true);

        // 如果已经尝试过，知道此规则不是规则类，则直接返回false
        if (isset($this->rulesClass[$ruleName]) && 0 === $this->rulesClass[$ruleName]) {
            return false;
        }

        // 猜测用户直接传入了规则类的完整命名空间
        if (($result = $this->tryGetRuleClass($ruleName, $params)) !== false) {
            return $result;
        }

        // 猜测用户传入了规则类的名字
        foreach (ValidateConfig::instance()->getRulePath() as $rulesPath) {
            $ruleNameSpace = $rulesPath . Str::studly($ruleName);
            if (($result = $this->tryGetRuleClass($ruleNameSpace, $params)) !== false) {
                return $result;
            }
        }

        $this->rulesClass[$ruleName] = 0;
        return false;
    }

    /**
     * 尝试通过规则名称获取规则类
     *
     * @param string     $ruleName 规则名称
     * @param array|null $params   参数
     *
     * @return BaseRule|false
     */
    private function tryGetRuleClass(string $ruleName, ?array $params = null): BaseRule|false
    {
        if (isset($this->rulesClass[$ruleName])) {
            return !is_null($params) ? new $ruleName(...$params) : new $ruleName();
        } elseif (class_exists($ruleName) && is_subclass_of($ruleName, BaseRule::class)) {
            $this->rulesClass[$ruleName] = 1;
            return !is_null($params) ? new $ruleName(...$params) : new $ruleName();
        }
        return false;
    }

    /**
     * 指定当前验证器的场景.
     *
     * @return $this
     */
    final public function scene(?string $name): static
    {
        $this->currentScene = $name;
        return $this;
    }

    /**
     * 获取当前验证器的场景名称.
     */
    public function getCurrentSceneName(): ?string
    {
        return $this->currentScene;
    }

    /**
     * 设置验证场景.
     *
     * @param array<string,array<string>|ValidateScene>|null $scene 如果为null，则清空所有验证场景
     */
    public function setScene(?array $scene = null): static
    {
        if (is_null($scene)) {
            $this->scene = [];
        } else {
            $this->scene = array_merge($this->scene, $scene);
        }

        return $this;
    }

    /**
     * 设置验证规则.
     *
     * @param array<string,string,array<string>>|null $rules 验证规则，如果为null，则清空所有验证规则
     *
     * @return $this
     */
    public function setRules(?array $rules = null): static
    {
        if (is_null($rules)) {
            $this->rule = [];
        } else {
            $this->rule = array_merge($this->rule, $rules);
        }

        return $this;
    }

    /**
     * 设置验证消息.
     *
     * @param array<string,string|array>|null $message 验证消息，如果为null，则清空所有验证消息
     *
     * @return $this
     */
    public function setMessages(?array $message = null): static
    {
        if (is_null($message)) {
            $this->message = [];
        } else {
            $this->message = array_merge($this->message, $message);
        }

        return $this;
    }

    /**
     * 设置验证字段名称.
     *
     * @param array<string,string>|null $customAttributes 验证字段名称，如果为null，则清空所有验证字段名称
     *
     * @return $this
     */
    public function setCustomAttributes(?array $customAttributes = null): static
    {
        if (is_null($customAttributes)) {
            $this->customAttributes = [];
        } else {
            $this->customAttributes = array_merge($this->customAttributes, $customAttributes);
        }

        return $this;
    }

    public function getMessages(): array
    {
        return $this->message;
    }

    public function getCustomAttributes(): array
    {
        return $this->customAttributes;
    }

    protected function getException(ValidationException $e)
    {
        $messageProvider = $this->getMessageProvider();
        $attribute       = $e->getAttribute();
        $rule            = $e->getRule();
        $attributeRule   = strtolower($attribute . '.' . $rule);
        $messageProvider->setAttribute($attribute)->setRule($rule)->setDisplayableAttribute($e->displayableAttribute);
        $error = $messageProvider->handleMessage($e->getMessage());

        $exceptionClass = null;
        if (is_string($this->exceptions) && class_exists($this->exceptions) && is_subclass_of($this->exceptions, Throwable::class)) {
            $exceptionClass = $this->exceptions;
        } elseif (is_array($this->exceptions) && !empty($this->exceptions)) {
            foreach ($this->exceptions as $exception => $keys) {
                if (!is_array($keys)) {
                    $keys = [$keys];
                }
                foreach ($keys as $key) {
                    if ($key === $attribute || strtolower($key) === $attributeRule) {
                        $exceptionClass = $exception;
                        break 2;
                    }
                }
            }
        }

        if (is_null($exceptionClass)) {
            return new ValidateException($error, 403, $e->getAttribute(), $e);
        } else {
            return new $exceptionClass(message: $error, code: 403, previous: $e);
        }

    }

    /**
     * 使用闭包方法来自定义验证场景并进行验证
     *
     * @throws ValidateException
     */
    public function invokeSceneCheck(callable $fn, array $data): array
    {
        $this->init();
        $this->checkData = $data;
        $this->addEvent('event', $this->event);
        $this->handleEvent($data, 'beforeValidate');
        $events       = $this->events;
        $this->events = [];

        $rules = $this->invokeSceneFunction($fn, $this->rule, $this->checkData);

        $data         = $this->pass($data, $rules);
        $this->events = $events;
        $this->handleEvent($data, 'afterValidate');
        return $data;
    }

    /**
     * Calling custom scene closures.
     */
    private function invokeSceneFunction(callable $fn, array $rule, array $data, string $sceneName = ''): array
    {
        $scene = new ValidateScene($rule, $data);
        call_user_func($fn, $scene);
        return $this->mergeSceneData($scene, $sceneName);
    }

    /**
     * 使用当前验证器下的数据来创建一个场景类.
     */
    public function makeValidateScene(): ValidateScene
    {
        return new ValidateScene($this->rule, $this->checkData);
    }

    /**
     * 合并验证场景数据到验证器中，仅在下次的自动验证生效.
     */
    private function mergeSceneData(ValidateScene $scene, string $sceneName = ''): array
    {
        $this->events         = $scene->events;
        $this->afters         = $scene->afters;
        $this->befores        = $scene->befores;
        $this->preprocessors  = $scene->preprocessors;
        $this->postprocessors = $scene->postprocessors;
        $this->eventPriority  = $scene->eventPriority;
        $next                 = $scene->next;
        $sceneRule            = $scene->getRules();
        if (!empty($next)) {
            return $this->next($scene->next, $sceneRule, $sceneName);
        }
        return $sceneRule;
    }

    /**
     * Get the rules that need to be validation in the scene.
     *
     * @param string|null $sceneName the scene name, or the current scene name if not provided
     *
     * @throws ValidateException
     */
    private function getSceneRules(?string $sceneName = ''): array
    {
        if ('' === $sceneName) {
            $sceneName = $this->getCurrentSceneName();
        }

        if (empty($sceneName)) {
            return $this->rule;
        }

        if (method_exists($this, 'scene' . Str::studly($sceneName))) {
            return $this->invokeSceneFunction([$this, 'scene' . Str::studly($sceneName)], $this->rule, $this->checkData, $sceneName);
        }

        if (isset($this->scene[$sceneName])) {
            $sceneRule = $this->scene[$sceneName];

            if ($sceneRule instanceof ValidateScene) {
                return $this->mergeSceneData($sceneRule, $sceneName);
            }

            foreach (['event', 'before', 'after'] as $eventType) {
                if (isset($sceneRule[$eventType])) {
                    $callback = $sceneRule[$eventType];
                    $this->addEvent($eventType, $callback);
                    unset($sceneRule[$eventType]);
                }
            }

            if (!empty($sceneRule['next'])) {
                $next = $sceneRule['next'];
                unset($sceneRule['next']);
                $rules = Common::getRulesAndFill($this->rule, $sceneRule);
                return $this->next($next, $rules, $sceneName);
            } else {
                return Common::getRulesAndFill($this->rule, $sceneRule);
            }
        }

        return $this->rule;
    }

    /**
     * Processing the next scene.
     *
     * @param string $next             Next scene name or scene selector
     * @param array  $rules            Validation rules
     * @param string $currentSceneName Current scene name
     *
     * @throws ValidateException
     */
    private function next(string $next, array $rules, string $currentSceneName = ''): array
    {
        if ($next === $currentSceneName) {
            throw new ValidateRuntimeException('The scene used cannot be the same as the current scene.');
        }

        // Pre-validation
        $data                = $this->pass($this->checkData, $rules);
        $this->validatedData = array_merge($this->validatedData, $data);

        // If a scene selector exists
        if (method_exists($this, Str::studly($next) . 'Selector')) {
            $next = call_user_func([$this, Str::studly($next) . 'Selector'], $this->validatedData);
            if (is_array($next)) {
                return Common::getRulesAndFill($this->rule, $next);
            }
        }

        if (empty($next)) {
            return [];
        }

        $this->scene($next);
        return $this->getSceneRules($next);
    }

    /**
     * Processing method.
     *
     * @param string $type 'before' or 'after'
     *
     * @throws ValidateException
     */
    private function handleEventCallback(array $data, string $type)
    {
        switch ($type) {
            case 'before':
                $callbacks = $this->befores;
                break;
            case 'after':
                $callbacks = $this->afters;
                break;
        }

        if (empty($callbacks)) {
            return;
        }

        foreach ($callbacks as $callback) {
            [$callback, $param] = $callback;
            if (!is_callable($callback)) {
                $callback = $type . Str::studly($callback);
                if (!method_exists($this, $callback)) {
                    throw new ValidateRuntimeException('Method Not Found');
                }
                $callback = [$this, $callback];
            }

            if (($result = call_user_func($callback, $data, ...$param)) !== true) {
                if (isset($this->message[$result])) {
                    $result = $this->getMessageProvider()->handleMessage($this->message[$result]);
                }
                throw new ValidateException($result, 403);
            }
        }
    }

    /**
     * validate event handling.
     *
     * @param array  $data   Validated data
     * @param string $method Event Name
     *
     * @throws ValidateException
     */
    private function handleEvent(array $data, string $method): void
    {
        if (empty($this->events)) {
            return;
        }

        foreach ($this->events as $events) {
            [$callback, $param] = $events;
            if (class_exists($callback) && is_subclass_of($callback, ValidateEventAbstract::class)) {
                /** @var ValidateEventAbstract $handler */
                $handler            = new $callback(...$param);
                $handler->sceneName = $this->getCurrentSceneName();
                $handler->data      = $data;
                if (true !== call_user_func([$handler, $method])) {
                    $message = $handler->message;
                    if (isset($this->message[$message])) {
                        $message = $this->getMessageProvider()->handleMessage($this->message[$message]);
                    }
                    throw new ValidateException($message, 403);
                }
            } else {
                throw new ValidateRuntimeException('Event error or nonexistence：' . $callback);
            }
        }
    }

    private function processData(array $data, array $fields, array $processors = []): array
    {
        if (empty($processors)) {
            return $data;
        }

        $newData    = validate_collect($data);
        $processors = array_intersect_key($processors, array_flip($fields));
        foreach ($processors as $field => $callback) {
            if (null === $callback) {
                continue;
            }

            $dataProcessors = $this->processDataCondition($callback);
            if (!is_array($dataProcessors)) {
                $dataProcessors = [$dataProcessors];
            }

            if (str_contains($field, '*')) {
                $flatData = ValidationData::initializeAndGatherData($field, $data);
                $pattern  = str_replace('\*', '[^\.]*', preg_quote($field, '/'));
                foreach ($flatData as $key => $value) {
                    if (Str::startsWith($key, $field) || preg_match('/^' . $pattern . '\z/', $key)) {
                        foreach ($dataProcessors as $dataProcessor) {
                            $this->dataProcessing($key, $dataProcessor, $newData);
                        }
                    }
                }
            } else {
                foreach ($dataProcessors as $dataProcessor) {
                    $this->dataProcessing($field, $dataProcessor, $newData);
                }
            }
        }

        return $newData->toArray();
    }

    /**
     * @return DataProcessor[]|DataProcessor
     */
    private function processDataCondition(mixed $callback): DataProcessor|array
    {
        $execCond       = [];
        $params         = [];
        $dataProcessors = [];
        $callbackType   = ProcessorValueType::AUTO;
        $dataProcessor  = new DataProcessor();
        if (is_array($callback) && !empty($callback)) {
            if (in_array(ProcessorOptions::MULTIPLE, $callback)) {
                foreach ($callback as $item) {
                    if ($item instanceof ProcessorSupport) {
                        continue;
                    }
                    $dataProcessor    = $this->processDataCondition($item);
                    $dataProcessors[] = $dataProcessor;
                }
                return $dataProcessors;
            }

            $callbackMain = array_shift($callback);

            foreach ($callback as $item) {
                if ($item instanceof ProcessorOptions) {
                    continue;
                }

                if ($item instanceof ProcessorValueType) {
                    $callbackType = $item;
                    continue;
                }

                if ($item instanceof ProcessorExecCond) {
                    $execCond[] = $item;
                    continue;
                }

                $params[] = $item;
            }

            $callback = $callbackMain;
        }

        $dataProcessor->setExecCond($execCond ?: [ProcessorExecCond::ALWAYS]);
        $dataProcessor->params       = $params;
        $dataProcessor->callback     = $callback;
        $dataProcessor->callbackType = $callbackType;
        return $dataProcessor;
    }

    /**
     * Applying preprocessor to the data.
     *
     * @param string        $field         Name of the data field to be processed
     * @param DataProcessor $dataProcessor Data processor
     * @param Collection    $data          Data to be processed
     */
    private function dataProcessing(
        string $field,
        DataProcessor $dataProcessor,
        Collection $data
    ): void {
        $isEmpty = function ($value) {
            return null === $value || [] === $value || '' === $value;
        };

        $execCondValue = $dataProcessor->execCond->value;
        $value         = $data->get($field);
        $has           = $data->has($field);

        if ((ProcessorExecCond::WHEN_EXIST->value & $execCondValue) === ProcessorExecCond::WHEN_EXIST->value && !$has) {
            return;
        }
        if ((ProcessorExecCond::WHEN_NOT_EXIST->value & $execCondValue) === ProcessorExecCond::WHEN_NOT_EXIST->value && $has) {
            return;
        }
        if ((ProcessorExecCond::WHEN_EMPTY->value & $execCondValue) === ProcessorExecCond::WHEN_EMPTY->value && !$isEmpty($value)) {
            return;
        }
        if ((ProcessorExecCond::WHEN_NOT_EMPTY->value & $execCondValue) === ProcessorExecCond::WHEN_NOT_EMPTY->value && $isEmpty($value)) {
            return;
        }

        $params        = $dataProcessor->params;
        $callback      = $dataProcessor->callback;
        $callbackType  = $dataProcessor->callbackType;
        $dataAttribute = new DataAttribute();

        $defaultCallParams = [$value, $field, $this->checkData, $dataAttribute];
        $callParams        = array_map(function ($param) use ($value, $field, $dataAttribute) {
            return match ($param) {
                ProcessorParams::Value         => $value,
                ProcessorParams::Attribute     => $field,
                ProcessorParams::OriginalData  => $this->checkData,
                ProcessorParams::DataAttribute => $dataAttribute,
                default                        => $param
            };
        }, $params);

        if (is_callable($callback) && (ProcessorValueType::FUNCTION === $callbackType || ProcessorValueType::AUTO === $callbackType)) {
            $value = call_user_func($callback, ...($callParams ?: $defaultCallParams));
        } elseif (
            (is_string($callback) || is_object($callback))
            && class_exists($callback)
            && is_subclass_of($callback, ProcessorInterface::class)
            && (ProcessorValueType::METHOD === $callbackType || ProcessorValueType::AUTO === $callbackType)
        ) {
            /** @var ProcessorInterface $callback */
            $callback = new $callback(...$callParams);
            $value    = $callback->handle($value, $field, $this->checkData);
        } elseif (
            is_string($callback)
            && method_exists($this, Str::studly($callback) . 'Processor')
            && (ProcessorValueType::METHOD === $callbackType || ProcessorValueType::AUTO === $callbackType)
        ) {
            $value = call_user_func([$this,  Str::studly($callback) . 'Processor'], ...($callParams ?: $defaultCallParams));
        } elseif (ProcessorValueType::VALUE === $callbackType || ProcessorValueType::AUTO === $callbackType) {
            $value = $callback;
        } else {
            throw new ValidateRuntimeException('The processor type is not supported');
        }

        if (true === $dataAttribute->deleteField) {
            $data->forget($field);
        } else {
            $data->set($field, $value);
        }
    }

    /**
     * 初始化验证器.
     */
    private function init(): void
    {
        $this->validatedData  = [];
        $this->validateFields = [];
        $this->initScene();
        $this->getSelfRules();
    }

    /**
     * 初始化验证场景.
     */
    private function initScene(): void
    {
        $this->afters         = [];
        $this->befores        = [];
        $this->events         = [];
        $this->preprocessors  = [];
        $this->postprocessors = [];
        $this->eventPriority  = true;
    }

    /**
     * 为验证器设置一个消息提供程序.
     *
     * @return $this
     *
     * @throws ValidateException
     */
    public function setMessageProvider(callable|MessageProviderInterface|string $messageProvider): static
    {
        if (is_string($messageProvider) && is_subclass_of($messageProvider, MessageProviderInterface::class)) {
            $this->messageProvider = new $messageProvider();
        } elseif (is_object($messageProvider) && is_subclass_of($messageProvider, MessageProviderInterface::class)) {
            $this->messageProvider = $messageProvider;
        } elseif (is_callable($messageProvider)) {
            $messageProvider = call_user_func($messageProvider);
            $this->setMessageProvider($messageProvider);
            return $this;
        } else {
            throw new ValidateRuntimeException('The provided message processor needs to implement the MessageProviderInterface interface');
        }

        return $this;
    }

    /**
     * 获取消息提供程序.
     */
    public function getMessageProvider(): MessageProviderInterface
    {
        if (empty($this->messageProvider)) {
            $this->messageProvider = new MessageProvider();
        }

        $messageProvider = $this->messageProvider;
        $messageProvider->setMessages($this->message);
        $messageProvider->setCustomAttributes($this->customAttributes);
        $messageProvider->setData($this->checkData);
        return $messageProvider;
    }

    /**
     * Add Event.
     *
     * @param string       $type     'event','before','after
     * @param string|array $callback
     */
    private function addEvent(string $type, $callback): void
    {
        $type .= 's';
        $callbacks = $this->$type;

        if (is_string($callback)) {
            $callbacks[] = [$callback, []];
        } else {
            foreach ($callback as $classOrMethod => $param) {
                if (is_int($classOrMethod)) {
                    $callbacks[] = [$param, []];
                } elseif (is_string($classOrMethod)) {
                    if (is_array($param)) {
                        $callbacks[] = [$classOrMethod, $param];
                    } else {
                        $callbacks[] = [$classOrMethod, [$param]];
                    }
                }
            }
        }

        $this->$type = $callbacks;
    }

    /**
     * 扩展规则.
     *
     * @see https://v.itwmw.com/6/Rule.html#extend-%E6%89%A9%E5%B1%95%E6%96%B9%E6%B3%95
     *
     * @return $this
     */
    public function extend(string $rule, callable|Closure|string $extension, ?string $message = null): static
    {
        $rule                    = Str::snake($rule);
        $this->extensions[$rule] = $extension;
        if (!is_null($message) && !isset($this->ruleMessage[$rule])) {
            $this->ruleMessage[$rule] = $message;
        }
        return $this;
    }

    /**
     * 扩展隐式规则.
     *
     * @see https://v.itwmw.com/6/Rule.html#extendimplicit-%E9%9A%90%E5%BC%8F%E6%89%A9%E5%B1%95
     *
     * @return $this
     */
    public function extendImplicit(string $rule, callable|Closure|string $extension, ?string $message = null): static
    {
        $rule = Str::snake($rule);
        if (!is_null($message) && !isset($this->ruleMessage[$rule])) {
            $this->ruleMessage[$rule] = $message;
        }
        $this->implicitExtensions[$rule] = $extension;
        return $this;
    }

    /**
     * 扩展依赖规则.
     *
     * @see https://v.itwmw.com/6/Rule.html#extenddependent-%E4%BE%9D%E8%B5%96%E6%80%A7%E9%AA%8C%E8%AF%81%E5%99%A8
     *
     * @return $this
     */
    public function extendDependent(string $rule, callable|Closure|string $extension, ?string $message = null): static
    {
        $rule = Str::snake($rule);
        if (!is_null($message) && !isset($this->ruleMessage[$rule])) {
            $this->ruleMessage[$rule] = $message;
        }
        $this->dependentExtensions[$rule] = $extension;
        return $this;
    }

    /**
     * 扩展错误信息替换器.
     *
     * @see https://v.itwmw.com/6/Rule.html#replacer-%E9%94%99%E8%AF%AF%E4%BF%A1%E6%81%AF%E6%9B%BF%E6%8D%A2%E5%99%A8
     *
     * @return $this
     */
    public function extendReplacer(string $rule, callable|Closure|string $replacer): static
    {
        $rule                   = Str::snake($rule);
        $this->replacers[$rule] = $replacer;
        return $this;
    }

    /**
     * 获取规则名称和参数
     *
     * @param string $value   规则
     * @param bool   $parsing 是否解析参数为数组
     *
     * @return array
     */
    protected function getKeyAndParam(string $value, bool $parsing = false): array
    {
        $param = null;
        if (str_contains($value, ':')) {
            $arg = explode(':', $value, 2);
            $key = $arg[0];
            if (count($arg) >= 2) {
                $param = $arg[1];
            }
        } else {
            $key = $value;
        }

        if ($parsing && !is_null($param)) {
            $param = preg_split('/,(?![^()]*\))/', $param);
        }
        return [$key, $param];
    }

    private function initRuleMessage(): void
    {
        foreach ($this->ruleMessage as $key => $message) {
            $snakeRule = Str::snake($key);
            if (isset($this->ruleMessage[$snakeRule])) {
                continue;
            }

            $studlyRule = Str::studly($key);
            if (isset($this->ruleMessage[$studlyRule])) {
                $this->ruleMessage[$snakeRule] = $this->ruleMessage[$studlyRule];
                unset($this->ruleMessage[$studlyRule]);
            }

            $camelRule = Str::camel($key);
            if (isset($this->ruleMessage[$camelRule])) {
                $this->ruleMessage[$snakeRule] = $this->ruleMessage[$camelRule];
                unset($this->ruleMessage[$camelRule]);
            }
        }
    }
}
