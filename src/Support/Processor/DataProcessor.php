<?php

namespace Itwmw\Validate\Support\Processor;

class DataProcessor
{
    public ProcessorExecCond $execCond;

    /**
     * @param mixed|null                            $callback
     * @param ProcessorExecCond[]|ProcessorExecCond $exec_cond
     * @param array                                 $params
     * @param ProcessorValueType                    $callbackType
     */
    public function __construct(
        public mixed $callback = null,
        array|ProcessorExecCond $exec_cond = ProcessorExecCond::ALWAYS,
        public array $params = [],
        public ProcessorValueType $callbackType = ProcessorValueType::AUTO
    ) {
        $this->setExecCond($exec_cond);
    }

    public function setExecCond(array|ProcessorExecCond $exec_cond): static
    {
        $flag = 0;
        if (is_array($exec_cond)) {
            foreach ($exec_cond as $item) {
                $flag |= $item->value;
            }
            $this->execCond = ProcessorExecCond::tryFrom($flag) ?: ProcessorExecCond::ALWAYS;
        } else {
            $this->execCond = $exec_cond;
        }
        return $this;
    }
}
