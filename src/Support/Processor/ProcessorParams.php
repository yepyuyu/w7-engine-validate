<?php

namespace Itwmw\Validate\Support\Processor;

enum ProcessorParams implements ProcessorSupport
{
    /** Provide a "Value" field for the closure. */
    case Value;

    /** Provide a "Attribute" field for the closure. */
    case Attribute;

    /** Provide a "OriginalData" field for the closure. */
    case OriginalData;

    /** Provide a "DataAttribute" field for the closure. */
    case DataAttribute;
}
