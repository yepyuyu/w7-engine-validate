<?php

namespace Itwmw\Validate\Support\Event;

interface ValidateEventInterface
{
    /**
     * Methods implemented prior to validation.
     */
    public function beforeValidate(): bool;

    /**
     * Methods implemented after validation.
     */
    public function afterValidate(): bool;
}
