<?php

namespace Itwmw\Validate\Support;

use Closure;
use Itwmw\Validate\Exception\ValidateRuntimeException;
use Itwmw\Validate\Support\Concerns\ProcessorInterface;
use Itwmw\Validate\Support\Processor\ProcessorExecCond;
use Itwmw\Validate\Support\Processor\ProcessorOptions;
use Itwmw\Validate\Support\Processor\ProcessorParams;
use Itwmw\Validate\Support\Processor\ProcessorSupport;
use Itwmw\Validate\Support\Processor\ProcessorValueType;
use Itwmw\Validate\Support\Rule\BaseRule;
use Itwmw\Validation\Support\Arr;
use Itwmw\Validation\Support\Collection\Collection;

/**
 * 验证器的场景类.
 *
 * @see https://v.itwmw.com/6/Scene.html#%E5%9C%BA%E6%99%AF%E7%B1%BB%E7%9A%84%E6%96%B9%E6%B3%95
 *
 * @property-read array  $events         本次验证需要处理的事件
 * @property-read array  $befores        本次验证之前需要处理的方法
 * @property-read array  $afters         本次验证之后需要处理的方法
 * @property-read array  $preprocessors  本次验证之前数据需要经过的处理器
 * @property-read array  $postprocessors 本次验证之后数据需要经过的处理器
 * @property-read bool   $eventPriority  本次验证的事件优先级
 * @property-read string $next           本次验证之后需要跳转的场景
 */
class ValidateScene
{
    /**
     * 本次验证需要处理的事件.
     */
    private array $events = [];

    /**
     * 本次验证之前需要处理的方法.
     */
    private array $befores = [];

    /**
     * 本次验证之后需要处理的方法.
     */
    private array $afters = [];

    /**
     * 本次验证之前数据需要经过的处理器.
     */
    private array $preprocessors = [];

    /**
     * 本次验证之后数据需要经过的处理器.
     */
    private array $postprocessors = [];

    /**
     * 本次验证的事件优先级.
     */
    private bool $eventPriority = true;

    /**
     * 本次验证之后需要跳转的场景.
     */
    private string $next = '';

    /**
     * 本次需要验证的规则.
     */
    protected array $checkRules = [];

    /**
     * @param array $rules     验证器提供的全部原始规则
     * @param array $checkData 用户提供的全部待验证数据
     */
    public function __construct(protected array $rules = [], protected array $checkData = [])
    {
    }

    /**
     * 指定场景需要验证的字段.
     *
     * @see https://v.itwmw.com/6/Scene.html#only
     *
     * @param array|true $fields 要验证的字段,当此值为true时，表示验证全部字段，当为false时，表示不验证任何字段
     *
     * @return $this
     */
    public function only(array|bool $fields): static
    {
        if (true === $fields) {
            $this->checkRules = $this->rules;
            return $this;
        }

        if (false === $fields) {
            return $this;
        }

        $this->checkRules = Common::getRulesAndFill($this->rules, $fields);
        return $this;
    }

    /**
     * 为场景中的字段需要追加验证规则.
     *
     * @see https://v.itwmw.com/6/Scene.html#append
     *
     * @param array|Closure|string $rules 支持追加规则名称，规则组，规则闭包，规则对象，类方法规则
     *
     * 如果你使用了闭包作为规则，闭包接收三个参数：字段名，字段值，验证失败回调，示例如下：
     * <code>
     * function ($attribute, $value, $fail) {
     *     if ($value === 'foo') {
     *         $fail('The '.$attribute.' is invalid.');
     *     }
     * },
     * </code>
     *
     * @return $this
     */
    public function append(string $field, array|Closure|string $rules): static
    {
        if (isset($this->checkRules[$field])) {
            if (empty($this->checkRules[$field])) {
                $this->checkRules[$field] = [];
            } elseif (!is_array($this->checkRules[$field])) {
                $this->checkRules[$field] = explode('|', $this->checkRules[$field]);
            }

            if (is_string($rules)) {
                $rules = explode('|', $rules);
                array_push($this->checkRules[$field], ...$rules);
            } else {
                $this->checkRules[$field][] = $rules;
            }
        }

        return $this;
    }

    /**
     * 删除字段的验证规则.
     *
     * @see https://v.itwmw.com/6/Scene.html#remove
     *
     * @param array|string|null $rule 移除的规则，多个规则用|分割，也可填入规则数组，如果$rule为null，则清空该字段下的所有规则
     *
     * @return $this
     */
    public function remove(string $field, array|string|null $rule = null): static
    {
        $removeRule = $rule;
        if (is_string($rule) && str_contains($rule, '|')) {
            $removeRule = explode('|', $rule);
        }

        if (is_array($removeRule)) {
            foreach ($removeRule as $rule) {
                $this->remove($field, $rule);
            }

            return $this;
        }

        if (isset($this->checkRules[$field])) {
            if (null === $rule) {
                $this->checkRules[$field] = [];
                return $this;
            }

            $rules = $this->checkRules[$field];

            if (is_string($rules)) {
                $rules = explode('|', $rules);
            }

            if (false !== ($index = strpos($rule, ':'))) {
                $rule = substr($rule, 0, $index);
            }

            $rules = array_filter($rules, function ($value) use ($rule) {
                if (str_contains($value, ':')) {
                    $value = substr($value, 0, strpos($value, ':'));
                }
                return $value !== $rule;
            });

            $this->checkRules[$field] = $rules;
        }

        return $this;
    }

    /**
     * 添加一个需要验证的字段.
     *
     * @see https://v.itwmw.com/6/Scene.html#appendcheckfield
     *
     * @return $this
     */
    public function appendCheckField(string $field): static
    {
        $rule             = $this->rules[$field] ?? '';
        $this->checkRules = array_merge($this->checkRules, [$field => $rule]);
        return $this;
    }

    /**
     * 删除一个不需要验证的字段.
     *
     * @see https://v.itwmw.com/6/Scene.html#removecheckfield
     *
     * @return $this
     */
    public function removeCheckField(string $field): static
    {
        unset($this->checkRules[$field]);
        return $this;
    }

    /**
     * 根据自定义条件增加规则.
     *
     * @see https://v.itwmw.com/6/Scene.html#sometimes
     *
     * @param string|string[]       $attribute 字段名称
     * @param array|string|BaseRule $rules     规则
     * @param callable              $callback  闭包，方法提供一个 {@see Collection} $data 参数,这是当前的验证数据
     *                                         如果返回 true，则为 $attribute 添加 $rules 中定义的规则
     *
     * @return $this
     */
    public function sometimes(array|string $attribute, BaseRule|array|string $rules, callable $callback): static
    {
        $data   = $this->getValidateData();
        $result = call_user_func($callback, $data);

        if (!$result) {
            return $this;
        }

        if (is_array($attribute)) {
            foreach ($attribute as $filed) {
                $this->append($filed, $rules);
            }
        } else {
            $this->append($attribute, $rules);
        }

        return $this;
    }

    /**
     * 场景事件.
     *
     * 此事件和全局事件共同存在
     *
     * @see https://v.itwmw.com/6/Scene.html#event
     *
     * @param string $handler   事件类命名空间，可使用:class进行传入
     * @param mixed  ...$params 传递给中间件构建方法的参数，不限数量
     *
     * @return $this
     */
    public function event(string $handler, ...$params): static
    {
        $this->events[] = [$handler, $params];
        return $this;
    }

    /**
     * 添加一个验证前的需要执行的方法，方法仅限本类的方法，方法的命名规则为 before 加方法名.
     *
     * @see https://v.itwmw.com/6/Scene.html#before
     *
     * @param callable|Closure|string $callbackName 本类的方法名,或{@see Closure}，{@see callable},传递给闭包的第一个值为数组类型的验证前的数据
     * @param mixed                   ...$params    要传递给方法的参数
     *
     * @return $this
     */
    public function before(callable|Closure|string $callbackName, ...$params): static
    {
        $this->befores[] = [$callbackName, $params];
        return $this;
    }

    /**
     * 添加一个验证后的需要执行的方法，方法仅限本类的方法，方法的命名规则为 after 加方法名.
     *
     * @see https://v.itwmw.com/6/Scene.html#after
     *
     * @param callable|Closure|string $callbackName 本类的方法名,或{@see Closure}，{@see callable}，传递给闭包的第一个值为数组类型的验证后的数据
     * @param mixed                   ...$params    要传递给方法的参数
     *
     * @return $this
     */
    public function after(callable|Closure|string $callbackName, ...$params): static
    {
        $this->afters[] = [$callbackName, $params];
        return $this;
    }

    /**
     * 为指定的字段设置前置数据处理器.
     *
     * @see https://v.itwmw.com/6/Scene.html#preprocessor
     *
     * @param string                                         $field    字段名称
     * @param callable|Closure|mixed|ProcessorInterface|null $callback 设置的前置处理器
     *
     * 你可以直接提供一个值，这个值会覆盖掉指定字段的值
     * 你可以通过添加 {@see ProcessorOptions}::WHEN_EMPTY 使其在值为空时生效
     * 也可以添加 {@see ProcessorOptions}::WHEN_NOT_EMPTY 使其在值不为空时生效
     * 此值还可以是一个 {@see ProcessorInterface} 的实例，一个{@see Closure}，一个{@see callable} 一个类方法，一个函数名称
     *
     * 匿名函数默认有四个参数:
     * <ul>
     * <li> `$value` 当前字段的数据</li>
     * <li> `$attribute` 当前字段的名称 </li>
     * <li> `$originalData` 当前验证器所有的原始验证数据 </li>
     * <li> `$dataAttribute` 一个{@see DataAttribute} 类</li>
     * </ul>
     *
     * 示例:
     * <code>
     * function($value,string $attribute,array $originalData, DataAttribute $dataAttribute){
     *     return $value;
     * }
     * </code>
     * 如果该参数为 null ,则删除该字段的所有前置数据处理器
     * @param ProcessorExecCond|ProcessorParams|ProcessorOptions|ProcessorValueType ...$params 自定义参数
     *
     * 如果你想自定义函数参数或者修改自定义函数的执行时机等，你可以使用 {@see ProcessorParams}，{@see ProcessorExecCond} 或者 {@see ProcessorOptions} 类
     * <b>注意:传入$callback参数的顺序与$params的顺序一致。</b>
     *
     * 如果你想指定`$callback`的类型，可以使用{@see ProcessorValueType}
     *
     * @return $this
     *
     * @noinspection PhpDocSignatureInspection
     */
    public function preprocessor(string $field, mixed $callback, ProcessorSupport ...$params): static
    {
        if (is_null($callback)) {
            $this->preprocessors[$field] = null;
        } else {
            if (!empty($params)) {
                $callback = [$callback, ...$params];
            }
            $this->preprocessors[$field] = $this->addProcessors($this->preprocessors[$field] ?? null, $callback);
        }
        return $this;
    }

    /**
     * 为指定的字段设置后置数据处理器.
     *
     * @see https://v.itwmw.com/6/Scene.html#postprocessor
     *
     * @param string                                         $field    字段名称
     * @param callable|Closure|mixed|ProcessorInterface|null $callback 设置的前置处理器
     *
     * 你可以直接提供一个值，这个值会覆盖掉指定字段的值
     * 你可以通过添加 {@see ProcessorOptions}::WHEN_EMPTY 使其在值为空时生效
     * 也可以添加 {@see ProcessorOptions}::WHEN_NOT_EMPTY 使其在值不为空时生效
     * 此值还可以是一个 {@see ProcessorInterface} 的实例，一个{@see Closure}，一个{@see callable} 一个类方法，一个函数名称
     *
     * 匿名函数默认有四个参数:
     * <ul>
     * <li> `$value` 当前字段的数据</li>
     * <li> `$attribute` 当前字段的名称 </li>
     * <li> `$originalData` 当前验证器所有的原始验证数据 </li>
     * <li> `$dataAttribute` 一个{@see DataAttribute} 类</li>
     * </ul>
     *
     * 示例:
     * <code>
     * function($value,string $attribute,array $originalData, DataAttribute $dataAttribute){
     *     return $value;
     * }
     * </code>
     * 如果该参数为 null ,则删除该字段的所有后置数据处理器
     *
     * @param ProcessorExecCond|ProcessorParams|ProcessorOptions|ProcessorValueType ...$params 自定义参数
     *
     * 如果你想自定义函数参数或者修改自定义函数的执行时机等，你可以使用 {@see ProcessorParams}，{@see ProcessorExecCond} 或者 {@see ProcessorOptions} 类
     * <b>注意:传入$callback参数的顺序与$params的顺序一致。</b>
     *
     * 如果你想指定`$callback`的类型，可以使用{@see ProcessorValueType}
     *

     *
     * @return $this
     *
     * @noinspection PhpDocSignatureInspection
     */
    public function postprocessor(string $field, mixed $callback, ProcessorSupport ...$params): static
    {
        if (is_null($callback)) {
            $this->postprocessors[$field] = null;
        } else {
            if (!empty($params)) {
                $callback = [$callback, ...$params];
            }
            $this->postprocessors[$field] = $this->addProcessors($this->postprocessors[$field] ?? null, $callback);
        }
        return $this;
    }

    private function addProcessors(mixed $processors, mixed $handler): array
    {
        if (empty($processors)) {
            $processors = [ProcessorOptions::MULTIPLE];
        } else {
            if (!is_array($processors)) {
                $processors = [ProcessorOptions::MULTIPLE, $processors];
            } elseif (!in_array(ProcessorOptions::MULTIPLE, $processors)) {
                array_unshift($processors, ProcessorOptions::MULTIPLE);
            }
        }
        $processors[] = $handler;
        return $processors;
    }

    /**
     * 设置事件和简易事件的优先级.
     *
     * 当值为True时，执行顺序为：事件类beforeValidate->简易事件before->开始数据验证->简易事件after->事件类afterValidate
     *
     * 当值为False时，执行顺序为：简易事件before->事件类beforeValidate->开始数据验证->事件类afterValidate->简易事件after
     *
     * @see https://v.itwmw.com/6/Scene.html#postprocessor
     *
     * @return $this
     */
    public function setEventPriority(bool $priority): static
    {
        $this->eventPriority = $priority;
        return $this;
    }

    /**
     * 指定本次验证完成之后自动跳转的下一个场景名或者场景选择器.
     *
     * @see https://v.itwmw.com/6/Scene.html#next
     *
     * @return $this
     */
    public function next(string $name): static
    {
        $this->next = $name;
        return $this;
    }

    /**
     * 获取当前验证的数据.
     *
     * @see https://v.itwmw.com/6/Scene.html#getdata
     *
     * @param string     $key     字段名称，如果为空则返回所有验证数据
     * @param mixed|null $default 默认值
     *
     * @return array|mixed
     */
    public function getData(string $key = '', mixed $default = null): mixed
    {
        if (!empty($key)) {
            return Arr::get($this->checkData, $key, $default);
        }
        return $this->checkData;
    }

    /**
     * 此方法用来获取当前验证的数据,作用同getData，区别在于，此方法返回一个验证器集合类.
     *
     * @see https://v.itwmw.com/6/Scene.html#getvalidatedata
     */
    public function getValidateData(): Collection
    {
        return validate_collect($this->getData());
    }

    /**
     * 获取当前场景的验证规则.
     */
    public function getRules(): array
    {
        return $this->checkRules;
    }

    public function __get($name)
    {
        if (property_exists($this, $name)) {
            return $this->$name;
        }

        throw new ValidateRuntimeException('Unknown property:' . $name);
    }
}
