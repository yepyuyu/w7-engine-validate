<?php

namespace Itwmw\Validate\Support\Concerns;

interface MessageProviderInterface
{
    /**
     * 错误消息处理.
     */
    public function handleMessage(string $messages): string;

    /**
     * 设置验证失败的属性.
     *
     * @return $this
     */
    public function setAttribute(string $attribute): static;

    /**
     * 设置验证失败的属性名称
     *
     * @param string $displayableAttribute
     *
     * @return $this
     */
    public function setDisplayableAttribute(string $displayableAttribute): static;

    /**
     * 设置验证失败的规则名称.
     *
     * @return $this
     */
    public function setRule(string $rule): static;

    /**
     * 获取错误消息.
     */
    public function getMessage(string $key, ?string $rule = null): ?string;

    /**
     * 获取原始错误消息.
     */
    public function getInitialMessage(?string $key = null, ?string $rule = null): string|array;

    /**
     * 设置错误消息集合.
     *
     * @return mixed
     */
    public function setMessages(array $messages): static;

    /**
     * 设置自定义属性名称集合.
     *
     * @param array<string, string> $customAttributes
     *
     * @return MessageProviderInterface
     */
    public function setCustomAttributes(array $customAttributes): static;

    /**
     * 设置验证数据.
     *
     * @return MessageProviderInterface
     */
    public function setData(array $data): static;
}
