<?php

namespace Itwmw\Validate\Support\Storage;

use Itwmw\Validation\Factory;
use Itwmw\Validation\Support\Interfaces\PresenceVerifierInterface;
use Itwmw\Validation\Support\Translation\Interfaces\Translator;

final class ValidateConfig
{
    /**
     * Custom rules namespace prefixes.
     */
    protected array $rulesPath = [];

    /**
     * Translator.
     */
    protected ?Translator $translator = null;

    /**
     * Validator Factory.
     */
    protected ?Factory $factory = null;

    /**
     * Presence Validator.
     */
    protected PresenceVerifierInterface $verifier;

    protected static ValidateConfig $instance;

    public static function instance(): ValidateConfig
    {
        if (empty(self::$instance)) {
            self::$instance = new ValidateConfig();
        }

        return self::$instance;
    }

    /**
     * Provide validator factory.
     *
     * @see https://v.itwmw.com/en/6/Start.html#configuration-validator-factory
     */
    public function setFactory(Factory $factory): ValidateConfig
    {
        $this->factory = $factory;
        return $this;
    }

    /**
     * Get Validator Factory.
     */
    public function getFactory(): Factory
    {
        if (empty($this->factory)) {
            $translator = $this->getTranslator();
            if (is_null($translator)) {
                $this->factory = new Factory();
            } else {
                $this->factory = new Factory($translator);
            }

            if ($this->getPresenceVerifier()) {
                $this->factory->setPresenceVerifier($this->getPresenceVerifier());
            }
        }

        return $this->factory;
    }

    /**
     * Set the presence verifier implementation.
     *
     * @return $this
     */
    public function setPresenceVerifier(PresenceVerifierInterface $presenceVerifier): ValidateConfig
    {
        $this->verifier = $presenceVerifier;
        return $this;
    }

    /**
     * Get presence verifier.
     */
    public function getPresenceVerifier(): ?PresenceVerifierInterface
    {
        return $this->verifier ?? null;
    }

    /**
     * Provide translator.
     *
     * @return $this
     */
    public function setTranslator(Translator $translator): ValidateConfig
    {
        $this->translator = $translator;
        return $this;
    }

    /**
     * Get Translator.
     */
    public function getTranslator(): ?Translator
    {
        return $this->translator;
    }

    /**
     * Set the custom rule namespace prefix, If more than one exists, they all take effect.
     *
     * @see https://v.itwmw.com/en/6/Rule.html#pre-processing
     *
     * @param string $rulesPath Custom rules namespace prefixes
     *
     * @return $this
     */
    public function setRulesPath(string $rulesPath): ValidateConfig
    {
        if (!str_ends_with($rulesPath, '\\')) {
            $rulesPath = $rulesPath . '\\';
        }
        $this->rulesPath[] = $rulesPath;
        $this->rulesPath   = array_unique($this->rulesPath);
        return $this;
    }

    /**
     * Get custom rules namespace prefixes.
     */
    public function getRulePath(): array
    {
        return $this->rulesPath;
    }
}
