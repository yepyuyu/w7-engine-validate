<?php

namespace PHPSTORM_META{
    expectedArguments(\Itwmw\Validate\Validate::addEvent(), 0, 'event', 'before', 'after');
    expectedArguments(\Itwmw\Validate\Validate::handleEventCallback(), 1, 'before', 'after');
}
