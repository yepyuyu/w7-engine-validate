<?php

namespace Itwmw\Validate\Tests\Test\Fixer;

use Itwmw\Validate\Exception\ValidateException;
use Itwmw\Validate\Support\ValidateScene;
use Itwmw\Validate\Tests\Material\BaseTestValidate;
use Itwmw\Validate\Tests\Material\Count;
use Itwmw\Validate\Validate;

class TestScene extends BaseTestValidate
{
    /**
     *  测试在场景中获取当前验证数据为空的问题
     *
     * @see https://gitee.com/yepyuyu/wm-validate/pulls/5
     *
     * @throws ValidateException
     */
    public function testSceneCheckDataIsEmpty()
    {
        $v = new class extends Validate {
            public $checkData = [];

            protected $rule = [
                'name' => 'required'
            ];

            protected function sceneTest(ValidateScene $scene)
            {
                $this->checkData = $scene->getData();
                $scene->only(['name']);
            }
        };

        $v->scene('test')->check([
            'name' => 123
        ]);

        $this->assertArrayHasKey('name', $v->checkData);
        $this->assertEquals(123, $v->checkData['name']);
    }

    /**
     *  测试场景中添加字段
     *
     * @see https://gitee.com/yepyuyu/wm-validate/pulls/3
     * @see https://gitee.com/yepyuyu/wm-validate/pulls/6
     *
     * @throws ValidateException
     */
    public function testSceneAppendCheckField()
    {
        $v = new class extends Validate {
            protected $rule = [
                'a' => 'required',
                'b' => 'required',
                'c' => 'required',
            ];

            protected $message = [
                'a.required' => 'a不能为空',
                'b.required' => 'b不能为空',
                'c.required' => 'c不能为空',
            ];

            protected function sceneTest(ValidateScene $scene)
            {
                $scene->only(['a'])
                    ->appendCheckField('b')
                    ->appendCheckField('c');
            }
        };

        try {
            $v->scene('test')->check([
                'a' => 1
            ]);
        } catch (ValidateException $e) {
            $this->assertEquals('b不能为空', $e->getMessage());
        }

        try {
            $v->scene('test')->check([
                'a' => 1,
                'b' => 1
            ]);
        } catch (ValidateException $e) {
            $this->assertEquals('c不能为空', $e->getMessage());
        }

        $data = $v->scene('test')->check([
            'a' => 1,
            'b' => 1,
            'c' => 1,
        ]);

        $this->assertEmpty(array_diff_key($data, array_flip(['a', 'b', 'c'])));
    }

    /**
     *  测试在next场景中，如果返回空场景名导致的问题
     *
     * @see https://gitee.com/yepyuyu/wm-validate/commit/db5ba9de603ac9e167fd46d2b90826595060813b
     *
     * @throws ValidateException
     */
    public function testNextSceneIsEmpty()
    {
        $v = new class extends Validate {
            protected $rule = [
                'a' => 'required'
            ];

            protected $scene = [
                'testA' => ['a', 'next' => 'test'],
            ];

            protected function testSelector(): string
            {
                Count::incremental('emptyScene');
                return '';
            }
        };

        $data = $v->scene('testA')->check(['a' => 1]);
        $this->assertEquals(1, Count::value('emptyScene'));
        $this->assertEquals(1, $data['a']);
    }

    /**
     *  测试在next中规则为原始规则的BUG
     *
     * @see https://gitee.com/yepyuyu/wm-validate/commit/f0cefc381e3dd90a8faf69514eb6a2d6016ede77
     *
     * @throws ValidateException
     */
    public function testRulesAreNotParsedForNext()
    {
        $v = new class extends Validate {
            protected $rule = [
                'a' => 'required|numeric|min:1|test',
                'b' => 'required|numeric|min:1',
            ];

            protected $scene = [
                'testA'    => ['a', 'next' => 'testNext'],
                'testNext' => ['a', 'b']
            ];

            protected function sceneTestB(ValidateScene $scene)
            {
                $scene->only(['a'])->next('testNext');
            }

            protected function ruleTest()
            {
                return true;
            }
        };

        $data = $v->scene('testA')->check(['a' => 2, 'b' => 3]);
        $this->assertEquals(2, $data['a']);

        $data = $v->scene('testB')->check(['a' => 2, 'b' => 3]);
        $this->assertEquals(2, $data['a']);
    }
}
