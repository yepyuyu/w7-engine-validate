<?php

namespace Itwmw\Validate\Tests\Test;

use Itwmw\Validate\Tests\Material\BaseTestValidate;
use Itwmw\Validate\Validate;

class TestException extends \Exception
{
}

class TestCustomException extends BaseTestValidate
{
    /**
     *  测试为验证失败的单一字段规则设置自定义异常
     */
    public function testValidateOneException()
    {
        $v = new class extends Validate {
            protected $rule = [
                'name' => 'required'
            ];

            protected $exceptions = [
                TestException::class => 'name.required'
            ];
        };

        $this->expectException(TestException::class);
        $this->expectExceptionMessage('Name不能为空。');
        $v->check([]);
    }

    /**
     *  测试为验证失败的单个字段下所有规则设置自定义异常
     */
    public function testValidateAllException()
    {
        $v = new class extends Validate {
            protected $rule = [
                'name' => 'required|integer'
            ];

            protected $exceptions = [
                TestException::class => 'name'
            ];
        };

        $this->expectException(TestException::class);
        $this->expectExceptionMessage('Name不能为空。');
        $v->check([]);
    }

    /**
     *  测试为验证失败的多个字段下的所有规则设置自定义异常
     */
    public function testValidateMultiException()
    {
        $v = new class extends Validate {
            protected $rule = [
                'name' => 'required|integer',
                'age'  => 'required|integer'
            ];

            protected $exceptions = [
                TestException::class => [
                    'name', 'age'
                ]
            ];
        };

        $this->expectException(TestException::class);
        $this->expectExceptionMessage('Age不能为空。');
        $v->check([
            'name' => 123
        ]);
    }

    /**
     *  测试为验证失败的多个字段下的指定规则设置自定义异常
     */
    public function testValidateMultiRuleException()
    {
        $v = new class extends Validate {
            protected $rule = [
                'name' => 'required|integer',
                'age'  => 'required|integer'
            ];

            protected $exceptions = [
                TestException::class => [
                    'name.required',
                    'age.required'
                ]
            ];
        };

        $this->expectException(TestException::class);
        $this->expectExceptionMessage('Age不能为空。');
        $v->check([
            'name' => 123
        ]);
    }

    /**
     *  为验证器设置一个全局的自定义异常
     */
    public function testValidateGlobalException()
    {
        $v = new class extends Validate {
            protected $rule = [
                'name' => 'required|integer'
            ];

            protected $exceptions = TestException::class;
        };

        $this->expectException(TestException::class);
        $this->expectExceptionMessage('Name必须是整数。');
        $v->check(['name' => 'abc']);
    }
}
