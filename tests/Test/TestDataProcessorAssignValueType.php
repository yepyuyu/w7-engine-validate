<?php

namespace Itwmw\Validate\Tests\Test;

use Itwmw\Validate\Support\Processor\ProcessorParams;
use Itwmw\Validate\Support\Processor\ProcessorValueType;
use Itwmw\Validate\Tests\Material\BaseTestValidate;
use Itwmw\Validate\Validate;

class TestDataProcessorAssignValueType extends BaseTestValidate
{
    /**
     * 测试为处理器的值指定类型
     */
    public function testProcessorValueIsValue()
    {
        $v = new class extends Validate {
            protected $rule = [
                'name' => 'string',
            ];

            protected $postprocessor = [
                'name' => ['trim', ProcessorParams::Value, ProcessorValueType::FUNCTION]
            ];
        };

        // 验证通过后，将trim指定为方法，然后将name的值进行trim处理
        $data = $v->check([
            'name' => '   test   '
        ]);
        $this->assertEquals('test', $data['name']);

        $v = new class extends Validate {
            protected $rule = [
                'name' => 'string',
            ];

            protected $postprocessor = [
                'name' => ['trim', ProcessorValueType::VALUE]
            ];
        };

        // 验证通过后，将name的值赋值为trim，表明这个trim是一个值，并不是一个方法
        $data = $v->check([
            'name' => '   name   '
        ]);
        $this->assertEquals('trim', $data['name']);

        $v = new class extends Validate {
            protected $rule = [
                'name' => 'string',
            ];

            protected $postprocessor = [
                'name' => ['trim', ProcessorValueType::METHOD]
            ];

            public function trimProcessor(mixed $value): string
            {
                return ' ' . $value . ' ';
            }
        };

        // 验证通过后，将name的值通过trimProcessor方法处理
        $data = $v->check([
            'name' => 'test'
        ]);
        $this->assertEquals(' test ', $data['name']);
    }
}
