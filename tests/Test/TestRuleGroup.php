<?php

namespace Itwmw\Validate\Tests\Test;

use Itwmw\Validate\Exception\ValidateException;
use Itwmw\Validate\Support\ValidateScene;
use Itwmw\Validate\Tests\Material\BaseTestValidate;
use Itwmw\Validate\Tests\Material\Rules\IsAdminRule;
use Itwmw\Validate\Validate;

class TestRuleGroup extends BaseTestValidate
{
    /**
     *  测试在规则中使用规则组
     *
     * @return void
     */
    public function testGroupForRule()
    {
        $v = new class extends Validate {
            protected $group = [
                'r'        => 'required',
                'username' => 'alpha_num|min:5|max:10'
            ];

            protected $rule = [
                'username' => 'r|username|string'
            ];
        };
        $this->assertEquals([
            'username' => ['required', 'alpha_num', 'min:5', 'max:10', 'string']
        ], $v->getCheckRules());
    }

    /**
     * 测试在自定义验证场景中使用规则组.
     *
     * @return void
     */
    public function testGroupForScene()
    {
        $v = new class extends Validate {
            protected $group = [
                'r'        => 'required',
                'username' => 'alpha_num|min:5|max:10'
            ];

            protected function sceneTest(ValidateScene $scene)
            {
                $scene->only(['username'])
                    ->append('username', 'r|username|string');
            }
        };

        $this->assertEquals([
            'username' => ['required', 'alpha_num', 'min:5', 'max:10', 'string']
        ], $v->scene('test')->getCheckRules());
    }

    /**
     * 测试规则组中包含自定义规则.
     */
    public function testGroupContainCustomRule()
    {
        $v = new class extends Validate {
            protected $group = [
                'r'        => 'required',
                'username' => 'isAdminFunc'
            ];

            protected $rule = [
                'username' => 'r|username'
            ];

            protected $message = [
                'username.is_admin_func' => '用户名错误'
            ];

            public function ruleIsAdminFunc($attribute, $value): bool
            {
                return 'admin' === $value;
            }
        };

        $data = $v->check([
            'username' => 'admin'
        ]);
        $this->assertSame('admin', $data['username']);

        $this->expectException(ValidateException::class);
        $this->expectExceptionMessage('用户名错误');
        $v->check([
            'username' => '123'
        ]);
    }

    /**
     * 测试规则组中包含自定义规则类.
     */
    public function testGroupContainCustomRuleClass()
    {
        $v = new class extends Validate {
            protected $group = [
                'r'        => 'required',
                'username' => IsAdminRule::class
            ];

            protected $rule = [
                'username' => 'r|username'
            ];
        };

        $data = $v->check([
            'username' => 'admin'
        ]);
        $this->assertSame('admin', $data['username']);

        $this->expectException(ValidateException::class);
        $this->expectExceptionMessage('Username必须为admin');
        $v->check([
            'username' => '123'
        ]);
    }

    /**
     * 测试规则组中包含正则表达式规则.
     */
    public function testGroupContainRegexRule()
    {
        $v = new class extends Validate {
            protected $regex = [
                'mobile' => '/^1[3-9]\d{9}$/',
            ];

            protected $group = [
                'username_format' => 'regex:mobile'
            ];

            protected $rule = [
                'username' => 'required|username_format'
            ];
        };

        $data = $v->check([
            'username' => '13122223333'
        ]);
        $this->assertSame('13122223333', $data['username']);

        $this->expectException(ValidateException::class);
        $this->expectExceptionMessage('Username格式不正确。');
        $v->check([
            'username' => '123'
        ]);
    }
}
