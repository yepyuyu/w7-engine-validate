<?php

namespace Itwmw\Validate\Tests\Material;

class Queue
{
    protected static $data = [];

    public static function push(string $name, $value): int
    {
        if (!is_array(self::$data[$name] ?? null)) {
            self::$data[$name] = [];
        }
        return array_push(self::$data[$name], $value);
    }

    public static function shift(string $name)
    {
        if (!is_array(self::$data[$name] ?? null)) {
            return null;
        }
        return array_shift(self::$data[$name]);
    }

    public static function count(string $name)
    {
        return \count(self::$data[$name] ?? []);
    }

    public static function clear(string $name)
    {
        self::$data[$name] = [];
    }
}
