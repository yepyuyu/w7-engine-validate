<?php

namespace Itwmw\Validate\Tests\Material;

use Itwmw\Validate\Support\ValidateScene;
use Itwmw\Validate\Validate;

class UserRulesManager extends Validate
{
    protected $rule = [
        'user'    => 'required|email',
        'pass'    => 'required|lengthBetween:6,16',
        'name'    => 'required|chs_custom|lengthBetween:2,4',
        'remark'  => 'required|alpha_dash',
        'captcha' => 'required|length:4|checkCaptcha',
    ];

    protected $scene = [
        'login'   => ['user', 'pass'],
        'captcha' => ['captcha']
    ];

    protected $customAttributes = [
        'user'    => '用户名',
        'pass'    => '密码',
        'name'    => '昵称',
        'remark'  => '备注',
        'captcha' => '验证码',
    ];

    protected $message = [
        'captcha.checkCaptcha' => '验证码错误',
        'user.email'           => '用户名必须为邮箱',
        'pass.lengthBetween'   => '密码长度错误'
    ];

    protected function sceneRegister(ValidateScene $scene)
    {
        return $scene->only(['user', 'pass', 'name', 'remark'])
            ->remove('remark', 'required|alpha_dash')
            ->append('remark', 'chs_custom');
    }

    protected function sceneRegisterNeedCaptcha(ValidateScene $scene)
    {
        return $this->sceneRegister($scene)->appendCheckField('captcha');
    }

    public function ruleCheckCaptcha($att, $value): bool
    {
        return true;
    }
}
