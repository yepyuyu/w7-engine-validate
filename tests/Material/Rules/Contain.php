<?php

namespace Itwmw\Validate\Tests\Material\Rules;

use Itwmw\Validate\Support\Rule\BaseRule;

class Contain extends BaseRule
{
    protected $message = ':attribute 中必须包含 %{needle}';

    protected $needle = '';

    public function __construct(string $needle)
    {
        $this->needle = $needle;
    }

    public function passes($attribute, $value): bool
    {
        return str_contains($value, $this->needle);
    }
}
