<?php

namespace Itwmw\Validate\Tests\Material\Rules;

use Itwmw\Validate\Support\Rule\BaseRule;

/**
 * 通过字节的方式来限定长度的合法范围.
 */
class LengthBetween extends BaseRule
{
    protected $message = ':attribute的长度需为%d~%d字节之间';

    protected $min;

    protected $max;

    public function __construct(int $min, int $max)
    {
        $this->min          = $min;
        $this->max          = $max;
        $this->messageParam = [$min, $max];
    }

    public function passes($attribute, $value): bool
    {
        return strlen($value) >= $this->min && strlen($value) <= $this->max;
    }
}
