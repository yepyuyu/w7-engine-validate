<?php

namespace Itwmw\Validate\Tests\Material\Rules;

use Itwmw\Validate\Support\Rule\BaseRule;

/**
 * 通过字节的方式来限定长度.
 */
class Length extends BaseRule
{
    protected $message = ':attribute的长度需为%d个字节';

    protected $size;

    public function __construct(int $size)
    {
        $this->size         = $size;
        $this->messageParam = [$size];
    }

    public function passes($attribute, $value): bool
    {
        return strlen($value) === $this->size;
    }
}
